import { Component, Output, OnInit, EventEmitter} from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-multiplication',
  templateUrl: './multiplication.component.html',
  styleUrls: ['./multiplication.component.scss']
})
export class MultiplicationComponent implements OnInit {

  identForm!: FormGroup;
  isSubmitted = false;
  badLogin = false;
  table= [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
  multi = 0;

  @Output()leTab = new EventEmitter<string>();



  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      multi: new FormControl()
    });
  }
  get formControls(){return this.identForm.controls;}
  multiplication() {
    this.multi=this.identForm.get('multi')?.value;
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if(this.identForm.value.multi==''){
      this.badLogin = true;
      return;
    }else{
      const number = this.identForm.value.multi;
      for(let i=1; i<=10; i++){
        const result = i*number;
        console.log(`${number} * ${i} = ${result}`);
      }
      this.leTab.emit(this.identForm.value.multi);
    }
  }

}
