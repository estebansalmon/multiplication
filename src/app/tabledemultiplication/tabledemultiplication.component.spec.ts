import { ComponentFixture, TestBed } from '@angular/core/testing';

import { tabledemultiplicationComponent } from './tabledemultiplication.component';

describe('tabledemultiplicationComponent', () => {
  let component: tabledemultiplicationComponent;
  let fixture: ComponentFixture<tabledemultiplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ tabledemultiplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(tabledemultiplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
