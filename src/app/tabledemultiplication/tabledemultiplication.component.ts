import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-tabledemultiplication',
  templateUrl: './tabledemultiplication.component.html',
  styleUrls: ['./tabledemultiplication.component.scss']
})
export class tabledemultiplicationComponent implements OnInit {

  identForm!: FormGroup;
  isSubmitted = false;
  badLogin = false;
  multipli = 0;
  tab_mult= [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
  multi = 0;
  nbrTable = Array();

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      multipli: new FormControl()
    });

  }
  get formControls() { return this.identForm.controls; }
  tabmult() {
    this.multipli = this.identForm.get('multipli')?.value;
    console.log();
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if (this.identForm.value.multipli == '') {
      this.badLogin = true;
      return;
    } else {
      this.nbrTable=[];
      const number = this.identForm.value.multipli;
      for (let j = 1; j <= number; j++) {
        this.nbrTable.push(j);
        console.log(this.nbrTable);

        for (let i = 1; i <= 10; i++) {
          const result = i * j;
          console.log(`${j} * ${i} = ${result}`);
          console.log(j);

        }
      }
    }
  }
}
