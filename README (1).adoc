== Introduction

L’exercice de programmation que l’on nous a porposée pour objectif de  permettre de consolider les premiers concepts présentés dans les premiers TD d’introduction à Angular.

== Prérequis :

Avoir étudié/réalisé les travaux dirigés de 1.1 à 2.3 (sur vinsio)

  ● Environnement de travail opérationnel
  ● Création d’un projet et de ses composants.
  ● Avoir Angular d'installer

== Objectif :

On souhaite concevoir une application web qui affiche une table de multiplication (1 à 10), selon
une valeur soumise par l’utilisateur.

== Analyse du code

Pour présenter ce code, nous allons suivre le scénario typique d'utilisation. Dans un premier temps, voici le `app.component.html`  celui ci nous permet d'afficher les composants `app-multiplication` et `app-TabledeMultiplication`
[, html]
----
<app-header></app-header>
<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="#">
      <img src="assets/images/calc.jpg" />
    </a>
  </div>
  <div id="navbarBasic" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item">Accueil</a>
    </div>
  </div>
</nav>
<section class="section">
  <div class="container" align="center">
    <div class="column">
      <h1 class="title is-1" >Tables de Multiplications</h1>
    </div>
  </div>
  <app-multiplication></app-multiplication>
  <app-tabledemultiplication></app-tabledemultiplication>
</section>
<app-footer></app-footer>
----

Puis on cree les formulaire qui permette de demander a l'utilisteur de rentrer le chiffre dont il souhaite affiché le tableau puis la boucle *ngif=multi qui nous permet de ne pas afficher celui ci aucun chiffre n'es rentrée et pour finir la boucle `ngfor` qui affiche le tableau du chiffre demandé si ngif est respecté
[, html]
----
<section class="section">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <form [formGroup]="identForm" (ngSubmit)="multiplication()">
                    <div class="field">
                        <label class="label">entrer un chiffre</label>
                        <div class="control">
                            <input class="input is-success" formControlName="multi" type="number" placeholder="Exemple : 5" value="">
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link">Soumettre</button>
                        </div>
                    </div>
                </form>
                <ul *ngIf=multi>
                    <li *ngFor="let numbers of table">
                        {{numbers.num}} x {{multi}} = {{numbers.num * multi}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</sect
----

[, php]
----

table= [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
----
ce code permet de cree le tableau qui contient tout les coefficient de multiplication

[, Java]
----
get formControls(){return this.identForm.controls;}
  multiplication() {
    this.multi=this.identForm.get('multi')?.value;
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if(this.identForm.value.multi==''){
      this.badLogin = true;
      return;
    }else{
      const number = this.identForm.value.multi;
      for(let i=1; i<=10; i++){
        const result = i*number;
        console.log(`${number} * ${i} = ${result}`);
      }
      this.leTab.emit(this.identForm.value.multi);
    }
  }

}
----
voici le code qui nous permet de recuperer  les valeur du formulaire de identform  puis de traiter les données

nous avons reussi a termine la partie 1 qui etait d'afficher le tableau du chiffre demandé lorsque l'utilisateur rentre un chiffre
image::unknown.png[]

== patie 2
pour cette partie il nous est demandé d'afficher la table de multiplication de 1 jusqu'a un chiffre demandé par l'utilisateur

[, html]
----
<section class="section">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <form [formGroup]="identForm" (ngSubmit)="tabmult()">
                    <div class="field">
                        <label class="label">entrer un chiffre</label>
                        <div class="control">
                            <input class="input is-success" formControlName="multipli" type="number" placeholder="Exemple : 5" value="">
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link">Soumettre</button>
                        </div>
                    </div>
                </form>
<ul *ngFor="let i of nbrTable">
    <li>----------------------</li>
    <li *ngFor="let numbers of tab_mult">
        {{nbrTable[i-1]}} x {{numbers.num}} = {{numbers.num * nbrTable[i-1]}}
    </li>
</ul>
----

voici le code du deuxieme formulaire qui lui nous permet l'affichage de multiple table

[,java]
----
 ngOnInit(): void {
    this.identForm = new FormGroup({
      multipli: new FormControl()
    });

  }
  get formControls() { return this.identForm.controls; }
  tabmult() {
    this.multipli = this.identForm.get('multipli')?.value;
    console.log();
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if (this.identForm.value.multipli == '') {
      this.badLogin = true;
      return;
    } else {
      this.nbrTable=[];
      const number = this.identForm.value.multipli;
      for (let j = 1; j <= number; j++) {
        this.nbrTable.push(j);
        console.log(this.nbrTable);

        for (let i = 1; i <= 10; i++) {
          const result = i * j;
          console.log(`${j} * ${i} = ${result}`);
          console.log(j);

        }
      }
    }
  }
}

----
ce code permet de recuperer le chiffre demandé puis on fait un boucle dans une boucle tant j  n'es pas egale au chiffre demander la boucle ne se ferme pas puis la deuxieme boucle effectue le calcule pour le renvoyer sur `TabledeMultiplication.component.html`

== Conclusion

Pour conclure, notre projet permet d'afficher la table de multiplication demander lorsque l'uitilisateur rentre un chiffre puis dans un second temps l'affichage des tableaux de 1 jusqu'au chiffre demandé

Liens vers mon dépôt Gitlab :https://gitlab.com/estebansalmon/multiplication




